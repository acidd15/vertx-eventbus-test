package io.vertx.blog.first;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

/**
 * Created by we on 2016. 10. 14..
 */
public class ConsumerVerticle extends AbstractVerticle {
    private Logger logger = LoggerFactory.getLogger(ConsumerVerticle.class);

    @Override
    public void start(Future<Void> fut) {
        EventBus eb = vertx.eventBus();

        eb.consumer("some_event", message -> {
            logger.info("CONSUMER : " + message.body() + " Received.");

            message.reply("Greetings!");

            logger.info("CONSUMER : I have sent greetings!");
        });

        logger.info("Another Verticle Ready!");
    }
}
