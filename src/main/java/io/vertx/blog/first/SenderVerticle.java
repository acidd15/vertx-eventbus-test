package io.vertx.blog.first;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

/**
 * Created by we on 2016. 10. 13..
 */
public class SenderVerticle extends AbstractVerticle {
    private Logger logger = LoggerFactory.getLogger(SenderVerticle.class);
    private int count = 0;

    @Override
    public void start(Future<Void> fut) {

        EventBus eb = vertx.eventBus();

        vertx.createHttpServer().requestHandler(r -> {
            logger.info("SENDER: Start to send.");

            eb.send("some_event", "Greetings!", ar -> {
                if (ar.succeeded()) {
                    if (ar.result().body().equals("Greetings!")) {
                        logger.info("SENDER: I have got your greetings!");

                        String data = "Hello World " + count;

                        logger.info("SENDER: Send to the client -> " + data);

                        r.response().end(data);
                        count++;
                    }
                } else {
                    logger.info(ar.cause());
                }
            });

            logger.info("SENDER: Send complete.");
        }).listen(8080, result -> {
            if (result.succeeded()) {
                fut.complete();
            } else {
                fut.fail(result.cause());
            }
        });

        logger.info("First Verticle Ready!");

    }
}
