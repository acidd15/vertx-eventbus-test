package io.vertx.blog.first;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

/**
 * Created by we on 2016. 10. 14..
 */
public class MainVerticle extends AbstractVerticle {
    private Logger logger = LoggerFactory.getLogger(MainVerticle.class);

    @Override
    public void start(Future<Void> fut) {
        vertx.deployVerticle(SenderVerticle.class.getName());
        vertx.deployVerticle(ConsumerVerticle.class.getName());
    }
}
