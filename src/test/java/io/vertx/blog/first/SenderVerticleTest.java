package io.vertx.blog.first;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by we on 2016. 10. 13..
 */
@RunWith(VertxUnitRunner.class)
public class SenderVerticleTest {

    private Vertx vertx;

    @Before
    public void setUp(TestContext context) {
        vertx = Vertx.vertx();
        DeploymentOptions options = new DeploymentOptions().setInstances(1);
        vertx.deployVerticle(SenderVerticle.class.getName(), options);
        vertx.deployVerticle(ConsumerVerticle.class.getName(), options);
    }

    @After
    public void tearDown(TestContext context) {
        vertx.close(context.asyncAssertSuccess());
    }

    @Test
    public void testMyApplication(TestContext context) {
        final Async async = context.async();

        vertx.createHttpClient().getNow(8080, "localhost", "/",
                response -> {
                    response.handler(
                            body -> {
                                context.assertTrue(body.toString().contains("Hello"));
                                async.complete();
                            }
                    );
                }
                );

    }
}
